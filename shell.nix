let
  pkgs = import <nixpkgs> {};
in pkgs.mkShell {
  buildInputs = (with pkgs; [
    nodejs
  ]) ++ (with pkgs.nodePackages; [
    eslint
    typescript-language-server
    typescript
  ]);
}
