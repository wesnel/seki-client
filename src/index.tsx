import React from "react";
import ReactDOM from "react-dom";

import App from "./components/App";

ReactDOM.render(
  <App apiURL={new URL(process.env.API_URL!)} />,
  document.getElementById("root")
);
