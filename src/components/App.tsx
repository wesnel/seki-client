import React, { ReactElement, useState } from "react";

import Game, { Stone } from "./Game";
import Initializer from "./Initializer";

let socket: WebSocket | null = null;

export default function App({ apiURL }: { apiURL: URL }): ReactElement {
  const [board, setBoard] = useState<Stone[][]>([]);
  const [currentTurn, setCurrentTurn] = useState<Stone>(Stone.Empty);
  const [playerColor, setPlayerColor] = useState<Stone>(Stone.Empty);

  // FIXME: Get these values from the server.
  const [rows, cols] = [19, 19];

  const offsetOf = (row: number, col: number): number => cols * row + col;

  const parseBoard = (flattened: Stone[]): Stone[][] => {
    const board: Stone[][] = [];

    for (let row = 0; row < rows; row++) {
      board.push([]);

      for (let col = 0; col < cols; col++) {
        const stone = flattened[offsetOf(row, col)];
        board[row].push(stone);
      }
    }

    return board;
  };

  const onOpen = (event: Event): void => {
    console.log(event);
  };

  const onClose = (event: CloseEvent): void => {
    console.log(event);
  };

  const onError = (event: Event): void => {
    console.error(event);
  };

  const onMessage = (event: MessageEvent): void => {
    console.log(event);

    const data = JSON.parse(event.data);
    console.log(data);
    const responseType = data.ResponseType;
    switch (responseType) {
      case "PlayedMoveResponse":
        setCurrentTurn(data.CurrentTurn);
        setBoard(parseBoard(data.Board));
        break;
      case "RegisterPlayerResponse":
        setPlayerColor(data.PlayerColor);
        setCurrentTurn(data.Properties.CurrentTurn);
        setBoard(parseBoard(data.Board));
        break;
      default:
        console.error(`invalid ResponseType from server: ${responseType}`);
        return;
    }
  };

  const onClick = (row: number, col: number): void => {
    if (socket !== null) {
      socket.send(
        JSON.stringify({
          Stone: playerColor,
          At: {
            Row: row,
            Col: col,
          },
        })
      );
    } else {
      console.error("asked to send data on an unopened socket");
    }
  };

  const joinGame = (gameID: string): void => {
    const socketURL = new URL(`${apiURL.toString()}play?id=${gameID}`);
    socketURL.protocol = "ws:";
    console.log(`joining game ${socketURL}`);

    try {
      socket = new WebSocket(socketURL.toString());

      socket.onopen = (event: Event) => onOpen(event);
      socket.onclose = (event: CloseEvent) => onClose(event);
      socket.onerror = (event: Event) => onError(event);
      socket.onmessage = (event: MessageEvent) => onMessage(event);
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <div className="app">
      <Initializer
        currentTurn={currentTurn}
        playerColor={playerColor}
        apiURL={apiURL}
        joinGame={(gameID: string): void => joinGame(gameID)}
        onError={(e: Error): void => {
          console.error(e);
        }}
      />
      <Game
        board={board}
        rows={rows}
        cols={cols}
        onClick={(row: number, col: number) => onClick(row, col)}
      />
    </div>
  );
}
