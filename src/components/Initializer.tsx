import React, { useState, ChangeEvent, MouseEvent, ReactElement } from "react";

import { Stone } from "./Game";

import "../css/initializer.css";

export default function Initializer({
  apiURL,
  currentTurn,
  playerColor,
  joinGame,
  onError,
}: {
  apiURL: URL;
  currentTurn: Stone;
  playerColor: Stone;
  joinGame: (gameID: string) => void;
  onError: (error: Error) => void;
}): ReactElement {
  const [gameID, setGameID] = useState<string>("");

  // Callback that fetches a new game ID from the API.
  const generateGame = (event: MouseEvent): void => {
    event.preventDefault();

    fetch(`${apiURL.toString()}games`, {
      method: "POST",
    })
      .then((response) => {
        console.log(response);
        return response.json();
      })
      .then((json) => {
        console.log(json);
        setGameID(json.GameID);
      })
      .catch((e) => {
        onError(e);
      });
  };

  return (
    <div className="initializer">
      <div className="current-turn">
        <span>{`current turn: ${currentTurn}`}</span>
      </div>
      <div className="player-color">
        <span>{`my stone color: ${playerColor}`}</span>
      </div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        <label htmlFor="gameID">
          {"to join an existing game, enter your game ID"}
        </label>
        <input
          id="gameID"
          type="text"
          value={gameID}
          onInput={(e: ChangeEvent<HTMLInputElement>): void => {
            e.preventDefault();
            setGameID(e.target.value);
          }}
        />
        <button onClick={(_) => joinGame(gameID)}>{"join this game"}</button>
        <button onClick={(e) => generateGame(e)}>
          {"generate a new game"}
        </button>
      </form>
    </div>
  );
}
