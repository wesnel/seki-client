import React, { ReactElement } from "react";

import Board from "./Board";

import "../css/game.css";

export enum Stone {
  Black = "b",
  White = "w",
  Empty = ".",
}

export default function Game({
  board,
  rows,
  cols,
  onClick,
}: {
  board: Stone[][];
  rows: number;
  cols: number;
  onClick: (row: number, col: number) => void;
}): ReactElement {
  return (
    <div className="game">
      <Board
        board={board}
        rows={rows}
        cols={cols}
        onClick={(row: number, col: number): void => {
          onClick(row, col);
        }}
      />
    </div>
  );
}
