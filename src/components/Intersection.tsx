import React, { ReactElement } from "react";

import { Stone } from "./Game";

import empty from "../css/resources/empty.svg";
import white from "../css/resources/white.svg";
import black from "../css/resources/black.svg";

export default function Intersection({
  stone,
  id,
  onClick,
}: {
  stone: Stone | null;
  id: string;
  onClick: () => void;
}): ReactElement {
  let svg = empty;

  switch (stone) {
    case Stone.Black:
      svg = black;
      break;
    case Stone.White:
      svg = white;
      break;
    default:
      break;
  }

  // TODO: Create different SVGs for the border of the game board.
  return (
    <button
      id={id}
      className="game-board-intersection"
      onClick={() => onClick()}
      style={{
        background: `url(${svg}) no-repeat scroll 0% 0% / contain`,
      }}
    ></button>
  );
}
