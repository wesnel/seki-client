import React, { ReactElement } from "react";

import { Stone } from "./Game";
import Intersection from "./Intersection";

export default function Board({
  board,
  rows,
  cols,
  onClick,
}: {
  board: Stone[][];
  rows: number;
  cols: number;
  onClick: (row: number, col: number) => void;
}): ReactElement {
  return (
    <div
      className="game-board"
      style={{
        display: "grid",
        gridTemplateColumns: `repeat(${cols}, calc(100% / ${cols}))`,
        gridTemplateRows: `repeat(${rows}, calc(100% / ${rows}))`,
      }}
    >
      {board.map((row, i) => (
        <div key={i} className="game-board-row" id={`game-board-row-${i}`}>
          {row.map((intersection, j) => (
            <Intersection
              key={j}
              id={`game-board-row-${i}-col-${j}`}
              stone={intersection}
              onClick={(): void => {
                onClick(i, j);
              }}
            />
          ))}
        </div>
      ))}
    </div>
  );
}
