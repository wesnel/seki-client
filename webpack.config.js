const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  entry: [
    `${__dirname}/src/index.tsx`,
  ],
  output: {
    path: `${__dirname}/build`,
    filename: '[name].[fullhash].js',
    publicPath: '/',
  },
  devServer: {
    contentBase: './build',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /\.svg$/,
        type: 'asset/inline',
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.css'],
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new HtmlWebpackPlugin({
      title: 'game of go',
      template: `${__dirname}/src/templates/index.html`,
    }),
    new webpack.EnvironmentPlugin({
      DEBUG: true,
      NODE_ENV: 'development',
      API_URL: 'http://localhost:8080',
    }),
  ],
};
